# Welcome to My Personal Developer Website!

## About Me

Hello and welcome to my personal developer website! I'm thrilled to have you here. My name is Daniel Makovsky, and I am a passionate developer with a love for creating innovative solutions and exploring new technologies.

## My Expertise

I specialize in cloud services, and serverless applications. With 3 years of experience in the field, I have had the opportunity to work on various projects, ranging from small personal websites to large-scale enterprise applications.

## Blog

I believe in sharing knowledge and experiences with the developer community. On my blog, you'll find articles on various topics related to software development, coding best practices, and industry trends. I love to write about serverless, frontend frameworks, machine learning algorithms and cybersecurity.

Visit my [blog](/blog) to read my latest articles and join the discussion.

## Contact Me

I'm always excited to connect with fellow developers, potential clients, or anyone interested in discussing ideas or collaboration opportunities. Feel free to reach out to me through the following channels:

- Email: [mdaniel@seznam.cz](mailto:mdaniel@seznam.cz)
- LinkedIn: [Daniel Makovský](https://cz.linkedin.com/in/daniel-makovsk%C3%BD-zakk)
- Twitter: [@NotSmartZakk](https://twitter.com/NotSmartZakk)

Thank you for visiting my website, and I hope you find it informative and inspiring. Let's create amazing things together!