# Really quick personal blog for SSP

Website is build on top of [MKDocs](https://gitlab.com/morph027/mkdocs)

## Requirements

To build or update website you need to have installed python and basic markdown knowledge.
Preferably with venv setted up.

All requirements are written in [requirements.txt](https://gitlab.com/eiguander/ssp_small/-/blob/main/requirements.txt)

## Building locally

To work locally with this project, you'll have to follow the steps below:

1. Fork, clone or download this project
1. [Install][] MkDocs
1. Preview your project: `mkdocs serve`,
   your site can be accessed under `localhost:8000`
1. Add content
1. Generate the website: `mkdocs build` (optional)

## Contributing

Although this git repository is completely public, please do not commit to this repository.

## Licensing

[MIT](https://cs.wikipedia.org/wiki/Licence_MIT)