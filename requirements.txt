mkdocs>=1.1.2
mkdocs-material>=9.1.6
mkdocs-material-extensions==1.1
mkdocs-minify-plugin==0.6.4
mkdocs-redirects==1.2.0
mkdocs-with-pdf==0.9.3
mkdocs-blogging-plugin==2.2.5
